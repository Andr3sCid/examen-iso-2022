package com.ufro.drogueria.Models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Trazabilidad {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @OneToOne
    @JoinColumn(name = "proveedor_id")
    private Proveedor proveedor;
    private String origen;
    private String destino;
}
