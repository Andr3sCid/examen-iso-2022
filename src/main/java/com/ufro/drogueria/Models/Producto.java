package com.ufro.drogueria.Models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Producto {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String principioActivo;
    private String nombreInsumos;
    private String formaFarmaceutica;
    private String fechaVencimineto;
    private int lote;
    private String proveedor;
    private String fechaRecepcion;
    private int precioUnitario;


}
