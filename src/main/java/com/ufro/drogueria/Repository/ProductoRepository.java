package com.ufro.drogueria.Repository;

import com.ufro.drogueria.Models.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductoRepository extends JpaRepository<Producto, Integer> {
}
