package com.ufro.drogueria.Repository;

import com.ufro.drogueria.Models.Proveedor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProveedorRepository extends JpaRepository<Proveedor, Integer> {
}
