package com.ufro.drogueria.Repository;

import com.ufro.drogueria.Models.Trazabilidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrazabilidadRepository extends JpaRepository<Trazabilidad, Integer> {
}
